library(tidyverse)
library(dplyr)
library(here)
library(ggplot2)

# Importation des données
femmes <- read_csv(here('data', 'femmes.csv'))

# Création des données pour l'histogramme
hist_data <- femmes %>%
  select(sexe, promotion_a) %>%
  mutate(promotion_a_2= case_when( # Recodage
          promotion_a <= 1976 ~ "Femmes entrées au corps des Mines 1975-1976",
          promotion_a <= 1978 ~ "Femmes entrées au corps des Mines 1977-1978",
          promotion_a <= 1980 ~ "Femmes entrées au corps des Mines 1979-1980",
          promotion_a <= 1982 ~ "Femmes entrées au corps des Mines 1981-1982",
          promotion_a <= 1984 ~ "Femmes entrées au corps des Mines 1983-1984",
          promotion_a <= 1986 ~ "Femmes entrées au corps des Mines 1985-1986",
          promotion_a <= 1988 ~ "Femmes entrées au corps des Mines 1987-1988",
          promotion_a <= 1990 ~ "Femmes entrées au corps des Mines 1989-1990",
          promotion_a <= 1992 ~ "Femmes entrées au corps des Mines 1991-1992",
          promotion_a <= 1994 ~ "Femmes entrées au corps des Mines 1993-1994",
          promotion_a <= 1996 ~ "Femmes entrées au corps des Mines 1995-1996",
          TRUE ~ as.character(promotion_a)
        )) %>%
  group_by(promotion_a_2, sexe) %>% # Regrouper par classe de promotion
  tally() # Compter

# Réalisation du graphique avec ggplot
hist <- ggplot(hist_data) +
  geom_bar(aes(promotion_a_2, n, fill=sexe), stat = "identity")

# Afficher l'histogramme
print(hist)
